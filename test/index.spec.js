const lab = (exports.lab = require("lab").script());

const { it, describe, before, beforeEach } = lab;
const { expect } = require("chai");

const Hapiness = require("../src/index");

describe("Hapiness Core Library", () => {
  let server;
  beforeEach(() => {
    server = new Hapiness();
  });
  describe("Initial server", () => {
    it("should have an empty plugins array", () => {
      expect(server.plugins).to.deep.equal([]);
    });
  });
  describe("register plugins", () => {
    it("should register plugins on call", () => {
      server.registerPlugins(["abc"]);
      expect(server.plugins).to.deep.equal(["abc"]);
    });
  });
});
